/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_chmod.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 13:37:50 by tlovato           #+#    #+#             */
/*   Updated: 2016/08/29 00:00:14 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void				disp_type(struct stat *st, char **ldisp)
{
	*ldisp = ft_strnew(1);
	if (S_ISREG(st->st_mode))
		*ldisp = ft_strcpy(*ldisp, "-");
	else if (S_ISDIR(st->st_mode))
		*ldisp = ft_strcpy(*ldisp, "d");
	else if (S_ISLNK(st->st_mode))
		*ldisp = ft_strcpy(*ldisp, "l");
	else if (S_ISFIFO(st->st_mode))
		*ldisp = ft_strcpy(*ldisp, "p");
	else if (S_ISCHR(st->st_mode))
		*ldisp = ft_strcpy(*ldisp, "c");
	else if (S_ISBLK(st->st_mode))
		*ldisp = ft_strcpy(*ldisp, "b");
}

static void			chmod_others(struct stat *st, char **ldisp)
{
	if (st->st_mode & S_IROTH)
		*ldisp = ft_strjoinfree(*ldisp, "r", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
	if (st->st_mode & S_IWOTH)
		*ldisp = ft_strjoinfree(*ldisp, "w", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
	if (st->st_mode & S_IXOTH)
		*ldisp = ft_strjoinfree(*ldisp, "x", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
}

static void			chmod_group(struct stat *st, char **ldisp)
{
	if (st->st_mode & S_IRGRP)
		*ldisp = ft_strjoinfree(*ldisp, "r", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
	if (st->st_mode & S_IWGRP)
		*ldisp = ft_strjoinfree(*ldisp, "w", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
	if (st->st_mode & S_IXGRP)
		*ldisp = ft_strjoinfree(*ldisp, "x", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
}

static void			chmod_user(struct stat *st, char **ldisp)
{
	if (st->st_mode & S_IRUSR)
		*ldisp = ft_strjoinfree(*ldisp, "r", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
	if (st->st_mode & S_IWUSR)
		*ldisp = ft_strjoinfree(*ldisp, "w", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
	if (st->st_mode & S_IXUSR)
		*ldisp = ft_strjoinfree(*ldisp, "x", 1);
	else
		*ldisp = ft_strjoinfree(*ldisp, "-", 1);
}

void				disp_chmod(struct stat *st, char **ldisp)
{
	chmod_user(st, ldisp);
	chmod_group(st, ldisp);
	chmod_others(st, ldisp);
}
