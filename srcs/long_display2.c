/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_display2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 20:06:12 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 14:58:15 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void			year_date(char **date, char **ldisp)
{
	char		*year;
	char		*cpydate;

	cpydate = *date;
	year = ft_strdup(&cpydate[ft_strlen(cpydate) - 5]);
	*date = ft_strsub(*date, 0, 11);
	*date = ft_strjoinfree(*date, " ", 1);
	*date = ft_strjoinfree(*date, year, 3);
	*ldisp = ft_strjoinfree(*ldisp, &(*date)[3], 1);
	(*ldisp)[ft_strlen(*ldisp) - 1] = '\0';
}

static void		maj_min(struct s_long *d, int ma, struct s_dir **s, int mi)
{
	put_len(d->malen, ma, *s);
	(*s)->ldisp = ft_strjoinfree((*s)->ldisp, ft_itoa(ma), 3);
	(*s)->ldisp = ft_strjoinfree((*s)->ldisp, ",", 1);
	put_len(d->milen + 1, mi, *s);
	(*s)->ldisp = ft_strjoinfree((*s)->ldisp, ft_itoa(mi), 3);
}

static void		init_datas2(time_t *ac, int *maj, int *min, struct s_long d)
{
	*ac = time(ac);
	*maj = d.st.st_rdev;
	*min = d.st.st_rdev;
}

void			put_last_infos(struct s_dir *sort, struct s_long *datas)
{
	char		*date;
	time_t		actime;
	int			major;
	int			minor;
	char		*tmp;

	init_datas2(&actime, &major, &minor, *datas);
	tmp = ft_itoa(datas->st.st_size);
	if (S_ISCHR(datas->st.st_mode) || S_ISBLK(datas->st.st_mode))
	{
		while ((major / 256) > 0)
			major = major / 256;
		minor = minor % 256;
		maj_min(datas, major, &sort, minor);
	}
	else
		sort->ldisp = ft_strjoinfree(sort->ldisp, tmp, 3);
	date = ctime(&datas->st.st_mtime);
	if (datas->st.st_mtime < actime - 15552000 || datas->st.st_mtime > actime)
		year_date(&date, &sort->ldisp);
	else
	{
		sort->ldisp = ft_strjoinfree(sort->ldisp, &date[3], 1);
		sort->ldisp[ft_strlen(sort->ldisp) - 9] = '\0';
	}
}
