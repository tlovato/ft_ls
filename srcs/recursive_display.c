/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   recursive_display.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 18:28:18 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 15:02:38 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void		do_stuff(DIR *d, struct s_dir **n, struct s_op *a, char *here)
{
	takedirs(d, n, *a, here);
	if (a->l || a->o || a->n)
		long_display(*n, here, *a);
	if (a->r)
		reverse(n);
	no_op_display(*n, a);
}

void		initrecdata(struct s_dir **new, char **here, char *path, char *n)
{
	char	*tmp;

	*new = NULL;
	if (ft_strcmp(path, "/") != 0)
		*here = ft_strjoin(path, "/");
	else
		*here = path;
	*here = ft_strjoin(*here, n);
}

void		if_new(DIR *dirp, struct stat st, struct s_dir **sort)
{
	if (dirp == NULL && !(st.st_mode & S_IRWXU) && (*sort)->next)
	{
		ft_putstr("ls: ");
		ft_putstr((*sort)->name);
		ft_putstr(": ");
		ft_putstr(strerror(errno));
		*sort = (*sort)->next;
	}
	if (closedir(dirp) == -1)
		exit(EXIT_FAILURE);
}

int			cond(struct s_op *args, struct stat *st, char **here, char *name)
{
	int		ret;

	ret = args->rmaj && S_ISDIR(st->st_mode) &&
		(!lstat(*here, st) && !S_ISLNK(st->st_mode)) &&
		(ft_strcmp(name, ".") != 0
		&& ft_strcmp(name, "..") != 0);
	return (ret);
}
