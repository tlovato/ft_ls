/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place_nodes.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 22:12:03 by tlovato           #+#    #+#             */
/*   Updated: 2016/08/22 02:01:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

struct s_dir		*free_lst(struct s_dir *lst)
{
	struct s_dir	*tmpnext;

	while (lst != NULL)
	{
		tmpnext = lst->next;
		if (lst->name)
			free(lst->name);
		if (lst->ldisp)
			free(lst->ldisp);
		if (lst->path)
			free(lst->path);
		lst->name = NULL;
		lst->ldisp = NULL;
		lst->path = NULL;
		free(lst);
		lst = tmpnext;
	}
	return (NULL);
}

void				add_first(struct s_dir **sort, struct s_dir *new)
{
	if (*sort != NULL && new != NULL)
	{
		new->next = *sort;
		*sort = new;
	}
	else
		*sort = new;
}

void				pl_node(struct s_dir **s, struct s_dir *new)
{
	struct s_dir	*ptr;

	ptr = *s;
	if (*s == NULL)
		*s = new;
	else if (ft_strcmp(new->name, (*s)->name) < 0)
		add_first(s, new);
	else
	{
		while (ptr->next != NULL)
		{
			if (ft_strcmp(new->name, ptr->next->name) < 0)
			{
				new->next = ptr->next;
				ptr->next = new;
				return ;
			}
			ptr = ptr->next;
		}
		ptr->next = new;
	}
}

void				time_sort1(struct s_dir *tmp, struct s_dir *new)
{
	while (tmp->next && new->st.st_mtime < tmp->next->st.st_mtime)
		tmp = tmp->next;
	new->next = tmp->next;
	tmp->next = new;
}

void				tsort2(struct s_dir *t, struct s_dir *n, struct s_dir **s)
{
	if (n->st.st_mtimespec.tv_nsec < t->st.st_mtimespec.tv_nsec)
	{
		while (t->next &&
				(n->st.st_mtimespec.tv_nsec < t->next->st.st_mtimespec.tv_nsec)
				&& n->st.st_mtime == t->next->st.st_mtime)
			t = t->next;
		n->next = t->next;
		t->next = n;
	}
	else if (n->st.st_mtimespec.tv_nsec > t->st.st_mtimespec.tv_nsec)
		add_first(s, n);
	else
	{
		while (t->next && ft_strcmp(n->name, t->next->name) < 0)
			t = t->next;
		n->next = t->next;
		t->next = n;
	}
}
