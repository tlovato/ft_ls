/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   linked_list.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/23 18:00:34 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 12:45:20 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

static void			ptime(struct s_dir *n, struct s_dir *tmp, struct s_dir **s)
{
	if (n->st.st_mtime < tmp->st.st_mtime)
		time_sort1(tmp, n);
	else if (n->st.st_mtime > tmp->st.st_mtime)
		add_first(s, n);
	else
		tsort2(tmp, n, s);
}

static void			time_sort(struct s_dir **sort, struct s_dir *new)
{
	struct s_dir	*tmp;

	tmp = *sort;
	if (*sort == NULL)
		*sort = new;
	else
	{
		while (tmp != NULL)
		{
			if (new->st.st_mtime < tmp->st.st_mtime
					|| new->st.st_mtime > tmp->st.st_mtime
					|| new->st.st_mtime == tmp->st.st_mtime)
			{
				ptime(new, tmp, sort);
				return ;
			}
			tmp = tmp->next;
		}
		tmp->next = new;
	}
}

void				stat_take_dirs(char *h, struct s_all *datas, int i)
{
	char			*npath;

	if (i)
		npath = ft_strjoin(h, datas->new->name);
	else
		npath = h;
	stat(npath, &datas->st);
	if (!lstat(npath, &datas->lst) && S_ISLNK(datas->lst.st_mode))
		datas->new->st = datas->lst;
	else
		datas->new->st = datas->st;
	if (i)
		free(npath);
}

struct s_dir		*takedirs(DIR *d, struct s_dir **s, struct s_op a, char *h)
{
	struct s_all	datas;
	char			*path;

	datas.dp = NULL;
	path = ft_strjoin(h, "/");
	while ((datas.dp = readdir(d)) != NULL)
	{
		while (!a.a && datas.dp->d_name[0] == '.')
		{
			datas.dp = readdir(d);
			if (datas.dp == NULL)
				return (datas.new = NULL);
		}
		if ((datas.new = (struct s_dir *)malloc(sizeof(struct s_dir))) == NULL)
			return (NULL);
		datas.new->name = ft_strdup(datas.dp->d_name);
		stat_take_dirs(path, &datas, 1);
		init_null(datas.new);
		if (a.t)
			time_sort(s, datas.new);
		else
			pl_node(s, datas.new);
	}
	free(path);
	return (*s);
}

struct s_dir		*take_lst(struct s_dir **list, struct s_op args)
{
	struct s_all	datas;
	int				i;

	i = (args.ndir == 0) ? 0 : 1;
	while (i <= args.ndir)
	{
		datas.new = (struct s_dir *)malloc(sizeof(struct s_dir));
		datas.new->name = ft_strdup(args.path[i]);
		stat_take_dirs(args.path[i], &datas, 0);
		free(args.path[i]);
		init_null(datas.new);
		if (args.t)
			time_sort(list, datas.new);
		else
			pl_node(list, datas.new);
		i++;
	}
	return (*list);
}
