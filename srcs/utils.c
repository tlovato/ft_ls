/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 18:43:44 by tlovato           #+#    #+#             */
/*   Updated: 2016/08/29 10:56:06 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

static void		malloc_tabpath(int ac, char **av, struct s_op *args)
{
	int			i;
	int			j;

	i = 1;
	j = 0;
	if (ac > 1)
		while (i < ac)
		{
			if (av[i][0] != '-')
				j++;
			i++;
		}
	args->ndir = j;
	args->path = (char **)malloc(sizeof(char *) * j);
}

char			**dir_name(int ac, char **av, struct s_op *args)
{
	int			i;
	int			j;

	malloc_tabpath(ac, av, args);
	j = 1;
	i = 1;
	args->path[0] = ft_strdup(".");
	if (ac > 1)
		while (i < ac)
		{
			if (av[i][0] != '-')
			{
				args->path[j] = ft_strdup(av[i]);
				j++;
			}
			i++;
		}
	return (args->path);
}

void			init_args(struct s_op *args)
{
	args->l = 0;
	args->rmaj = 0;
	args->r = 0;
	args->a = 0;
	args->t = 0;
	args->f = 0;
	args->n = 0;
	args->o = 0;
	args->gmaj = 0;
	args->invop = 0;
	args->path = NULL;
	args->ndir = 0;
	args->lst = NULL;
}

void			put_len(int len, int word, struct s_dir *sort)
{
	int			i;
	char		*tmp;

	i = len - ft_strlen(ft_itoa(word));
	tmp = (char *)malloc(sizeof(char) * (i + 1));
	ft_memset(tmp, ' ', i);
	tmp[i] = '\0';
	sort->ldisp = ft_strjoinfree(sort->ldisp, tmp, 3);
}

void			no_op_display(struct s_dir *sort, struct s_op *arg)
{
	while (sort != NULL)
	{
		while (sort && !arg->a && !arg->f && sort->name[0] == '.')
			sort = sort->next;
		if (arg->l || arg->o || arg->n)
		{
			ft_putstr(sort->ldisp);
			ft_putchar(' ');
		}
		if (sort)
		{
			if (arg->gmaj)
				pars_gmaj(sort);
			else
				ft_putendl(sort->name);
			sort = sort->next;
		}
	}
}
