/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 18:00:26 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 12:03:52 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

static void			ending_ls(struct s_op *args, DIR *dirp)
{
	if (dirp != NULL)
		if (closedir(dirp) == -1)
			exit(EXIT_FAILURE);
	free(args->path);
}

static void			pars_takedirs(struct s_op *a, struct s_dir **s, DIR *dirp)
{
	if (a->f)
		*s = f_option(dirp, a->lst->name);
	else
		*s = takedirs(dirp, s, *a, a->lst->name);
	pars_display(s, a, a->lst->name);
}

static void			init_datas(struct s_op *args, int ac, char **av)
{
	init_args(args);
	stock_options(ac, av, args);
	args->path = dir_name(ac, av, args);
	args->lst = take_lst(&args->lst, *args);
}

static void			free_end_ls(struct s_op *args)
{
	free(args->lst->ldisp);
	free(args->lst->path);
	free(args->lst->name);
	free(args->lst);
}

int					main(int ac, char **av)
{
	struct s_dir	*sort;
	struct s_op		args;
	struct stat		st;

	init_datas(&args, ac, av);
	while (args.lst)
	{
		sort = NULL;
		stat(args.lst->name, &st);
		if ((args.dirp = opendir(args.lst->name)) == NULL)
			cant_open(st, args.lst->name, args);
		else
		{
			if (args.ndir > 1 && S_ISDIR(st.st_mode))
				put_name(args.lst->name);
			pars_takedirs(&args, &sort, args.dirp);
			if (args.ndir > 1 && args.lst->next != NULL && S_ISDIR(st.st_mode))
				ft_putchar('\n');
		}
		args.tmp = args.lst->next;
		free_end_ls(&args);
		args.lst = args.tmp;
	}
	ending_ls(&args, args.dirp);
	return (EXIT_SUCCESS);
}
