/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 13:22:32 by tlovato           #+#    #+#             */
/*   Updated: 2016/08/22 13:22:34 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

int					bl_len(struct stat st, struct s_dir *s, char *path, int n)
{
	int				i;
	int				max;
	char			*new;
	int				nb;

	max = 0;
	while (s != NULL)
	{
		i = 0;
		new = ft_strjoin(path, s->name);
		stat(new, &st);
		nb = n ? st.st_size : st.st_nlink;
		while (nb)
		{
			nb = nb / 10;
			i++;
		}
		if (i > max)
			max = i;
		s = s->next;
	}
	return (max);
}

int					grusr_len(struct stat st, struct s_dir *s, char *p, int n)
{
	int				i;
	int				max;
	char			*new;
	struct group	*gr;
	struct passwd	*pw;

	max = 0;
	while (s != NULL)
	{
		i = 0;
		new = ft_strjoin(p, s->name);
		stat(new, &st);
		gr = getgrgid(st.st_gid);
		pw = getpwuid(st.st_uid);
		if (ft_strlen(n ? gr->gr_name : pw->pw_name) > max)
			max = ft_strlen(n ? gr->gr_name : pw->pw_name);
		s = s->next;
	}
	return (max);
}

int					minmaj_len(struct stat st, struct s_dir *s, char *p, int z)
{
	int				i;
	int				max;
	char			*new;
	int				minmaj;

	max = 1;
	while (s != NULL)
	{
		i = 0;
		new = ft_strjoin(p, s->name);
		stat(new, &st);
		if (z)
			minmaj = calc_maj(st.st_rdev);
		else
			minmaj = st.st_rdev % 256;
		i = calc_minmaj(minmaj);
		if (i > max)
			max = i;
		s = s->next;
		free(new);
	}
	return (max);
}

void				put_bl_len(int l, struct stat st, struct s_dir *s, int n)
{
	int				i;

	i = 0;
	while (i < l - ft_strlen(ft_itoa(n ? st.st_size : st.st_nlink)))
	{
		s->ldisp = ft_strjoin(s->ldisp, " ");
		i++;
	}
}

void				check_reg(DIR *dirp)
{
	struct dirent	*dp;
	struct stat		st;

	if ((dp = readdir(dirp)) != NULL)
	{
		stat(dp->d_name, &st);
		if (S_ISREG(st.st_mode))
			ft_printf("%s\n", dp->d_name);
	}
	else
	{
		ft_putstr("ls: ");
		ft_putstr(dp->d_name);
		ft_putstr(": ");
		ft_putstr(strerror(errno));
		exit(EXIT_FAILURE);
	}
}
