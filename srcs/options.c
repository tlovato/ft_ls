/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   options.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/11 17:23:44 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 15:23:59 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

struct s_dir		*f_option(DIR *dirp, char *here)
{
	struct s_all	datas;
	char			*path;

	datas.dp = NULL;
	path = ft_strjoin(here, "/");
	if ((datas.dp = readdir(dirp)) != NULL)
	{
		datas.new = (struct s_dir *)malloc(sizeof(struct s_dir));
		datas.new->name = ft_strdup(datas.dp->d_name);
		stat_take_dirs(path, &datas, 1);
		datas.new->ldisp = NULL;
		datas.new->path = NULL;
		datas.new->next = f_option(dirp, path);
		free(path);
		return (datas.new);
	}
	free(path);
	return (NULL);
}

void				pars_gmaj(struct s_dir *sort)
{
	if (S_ISLNK(sort->st.st_mode))
		ft_printf("\033[1;35m%s\033[m\n", sort->name);
	else if (S_ISDIR(sort->st.st_mode) && (sort->st.st_mode & S_IRWXU))
		ft_printf("\033[1;36m%s\033[m\n", sort->name);
	else if (S_ISFIFO(sort->st.st_mode) || S_ISBLK(sort->st.st_mode))
		ft_printf("\033[1;33m%s\033[m\n", sort->name);
	else if (!(sort->st.st_mode & S_IRWXU))
		ft_printf("\033[1;43m%s\033[m\n", sort->name);
	else
		ft_printf("%s\n", sort->name);
}

void				reverse(struct s_dir **sort)
{
	struct s_dir	*rev;
	struct s_dir	*first;

	rev = *sort;
	if (rev && rev->next)
	{
		first = rev->next;
		reverse(&(rev->next));
		first->next = rev;
		*sort = rev->next;
		first->next->next = NULL;
	}
}

void				pars_display(struct s_dir **s, struct s_op *a, char *path)
{
	if (a->r)
		reverse(s);
	if (a->l || a->n || a->o)
		long_display(*s, path, *a);
	no_op_display(*s, a);
	if (a->rmaj)
		rec_display(*s, a, path);
	*s = free_lst(*s);
}

void				rec_display(struct s_dir *s, struct s_op *args, char *path)
{
	struct s_rec	rec;

	while (s != NULL)
	{
		initrecdata(&rec.new, &rec.here, path, s->name);
		stat(rec.here, &rec.st);
		rec.condition = cond(args, &rec.st, &rec.here, s->name);
		if (rec.condition)
		{
			ft_putchar('\n');
			put_name(rec.here);
			if (!(rec.st.st_mode & S_IRWXU))
				put_perm_denied(s->name);
			else if ((rec.dirp = opendir(rec.here)) != NULL)
				do_stuff(rec.dirp, &rec.new, args, rec.here);
			if (rec.new)
			{
				if_new(rec.dirp, rec.st, &s);
				rec_display(rec.new, args, rec.here);
				rec.new = free_lst(rec.new);
			}
		}
		free(rec.here);
		s = s->next;
	}
}
