/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 18:41:00 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/08 18:41:01 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

char			*ft_strjoinfree(char *s1, char *s2, char i)
{
	char		*tmp;

	tmp = ft_strjoin(s1, s2);
	if (i == 1 || i == 3)
		free(s1);
	if (i == 2 || i == 3)
		free(s2);
	return (tmp);
}

void			put_name(char *name)
{
	ft_putstr(name);
	ft_putendl(":");
}

void			put_perm_denied(char *name)
{
	ft_putstr("ls :");
	ft_putstr(name);
	ft_putendl(": Permission denied");
}

void			init_null(struct s_dir *new)
{
	new->ldisp = NULL;
	new->path = NULL;
	new->next = NULL;
}

void			pinfnorme(struct s_long *d, struct s_op a, struct s_dir *s)
{
	struct group	*gr;
	char			*tmp;

	gr = getgrgid(d->st.st_gid);
	tmp = ft_itoa(gr->gr_gid);
	if (a.n)
		s->ldisp = ft_strjoinfree(s->ldisp, tmp, 1);
	else
		s->ldisp = ft_strjoinfree(s->ldisp, gr->gr_name, 1);
	add_spaces(&s->ldisp, d->glen, a.n ? tmp : gr->gr_name);
	free(tmp);
}
