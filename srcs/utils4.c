/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/08 19:59:21 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/08 19:59:23 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

int		calc_maj(int rdev)
{
	while ((rdev / 256) > 0)
		rdev /= 256;
	return (rdev);
}

int		calc_minmaj(int minmaj)
{
	int		i;

	i = 0;
	while (minmaj)
	{
		minmaj = minmaj / 10;
		i++;
	}
	return (i);
}
