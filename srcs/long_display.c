/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   long_display.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 13:11:51 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 12:42:28 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void				add_spaces(char **ldisp, int len, char *name)
{
	int				i;
	char			*tmp;

	i = len - ft_strlen(name);
	tmp = (char *)malloc(sizeof(char) * (i + 1));
	ft_memset(tmp, ' ', i);
	tmp[i] = '\0';
	*ldisp = ft_strjoinfree(*ldisp, tmp, 3);
}

static void			put_infos(struct s_dir *s, struct s_op a, struct s_long *d)
{
	struct passwd	*pw;
	char			*tmp;

	s->ldisp = ft_strjoinfree(s->ldisp, ft_itoa(d->st.st_nlink), 3);
	s->ldisp = ft_strjoinfree(s->ldisp, " ", 1);
	pw = getpwuid(d->st.st_uid);
	tmp = ft_itoa(pw->pw_uid);
	if (a.n)
		s->ldisp = ft_strjoinfree(s->ldisp, tmp, 1);
	else
		s->ldisp = ft_strjoinfree(s->ldisp, pw->pw_name, 1);
	add_spaces(&s->ldisp, d->ulen, a.n ? tmp : pw->pw_name);
	free(tmp);
	s->ldisp = ft_strjoinfree(s->ldisp, "  ", 1);
	if (!a.o)
		pinfnorme(d, a, s);
}

static int			is_symlink(struct s_dir *sort, char *new, struct stat *lst)
{
	char			*buf;

	if (!lstat(new, lst) && S_ISLNK(lst->st_mode))
	{
		buf = (char *)malloc(sizeof(char) * 100);
		ft_bzero(buf, 100);
		readlink(new, buf, 100);
		sort->name = ft_strjoinfree(sort->name, " -> ", 1);
		sort->name = ft_strjoinfree(sort->name, buf, 3);
		return (1);
	}
	return (0);
}

static void			init_datas(struct s_long *d, char *p, struct s_dir **sort)
{
	d->total = 0;
	d->llen = bl_len(d->st, *sort, p, 0) + 1;
	d->blen = bl_len(d->st, *sort, p, 1) + 2;
	d->ulen = grusr_len(d->st, *sort, p, 0);
	d->glen = grusr_len(d->st, *sort, p, 1);
	d->milen = minmaj_len(d->st, *sort, p, 0);
	d->malen = minmaj_len(d->st, *sort, p, 1);
}

void				long_display(struct s_dir *sort, char *p, struct s_op arg)
{
	struct s_long	datas;

	p = ft_strjoin(p, "/");
	init_datas(&datas, p, &sort);
	while (sort)
	{
		while (sort && !arg.a && sort->name[0] == '.')
			sort = sort->next;
		datas.new = ft_strjoin(p, sort->name);
		stat(datas.new, &datas.st);
		if (is_symlink(sort, datas.new, &datas.lst))
			datas.st = datas.lst;
		datas.total = datas.total + datas.st.st_blocks;
		disp_type(&datas.st, &sort->ldisp);
		disp_chmod(&datas.st, &sort->ldisp);
		put_bl_len(datas.llen, datas.st, sort, 0);
		put_infos(sort, arg, &datas);
		put_bl_len(datas.blen, datas.st, sort, 1);
		put_last_infos(sort, &datas);
		sort = sort->next;
	}
	ft_printf("total %d\n", datas.total);
}
