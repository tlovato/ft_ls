/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 21:49:14 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 12:07:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

void			cant_open(struct stat st, char *path, struct s_op args)
{
	if (S_ISREG(st.st_mode) || S_ISFIFO(st.st_mode) || S_ISCHR(st.st_mode)
		|| S_ISBLK(st.st_mode) || S_ISSOCK(st.st_mode))
		ft_putendl(path);
	else if (!lstat(path, &st) && S_ISLNK(st.st_mode))
		symlnk_case(st, path, args);
	else if (!(st.st_mode & S_IRWXU)
			&& (!lstat(path, &st) && !S_ISLNK(st.st_mode)))
	{
		ft_putstr("ls: ");
		ft_putstr(path);
		ft_putstr(": ");
		ft_putstr(strerror(errno));
		exit(EXIT_FAILURE);
	}
	else
	{
		ft_putstr("ls: ");
		ft_putstr(path);
		ft_putstr(": ");
		ft_putstr(strerror(errno));
		exit(EXIT_FAILURE);
	}
}

static int		check_op(char *option, int *inv)
{
	int			i;

	i = 0;
	while (option[i])
	{
		if (option[i] != 'a' && option[i] != 'l' && option[i] != 'R'
			&& option[i] != 'r' && option[i] != 't'
			&& option[i] != 'f' && option[i] != 'G' && option[i] != 'n'
			&& option[i] != 'o')
		{
			*inv = option[i];
			return (0);
		}
		i++;
	}
	return (1);
}

static void		pars1(struct s_op *args, char *str)
{
	if (ft_strrchr(str, 'a'))
		args->a = 1;
	if (ft_strrchr(str, 'l'))
		args->l = 1;
	if (ft_strrchr(str, 'R'))
		args->rmaj = 1;
	if (ft_strrchr(str, 'r'))
		args->r = 1;
	if (ft_strrchr(str, 't'))
		args->t = 1;
}

static void		pars2(struct s_op *args, char *str)
{
	if (ft_strrchr(str, 'f'))
		args->f = 1;
	if (ft_strrchr(str, 'G'))
		args->gmaj = 1;
	if (ft_strrchr(str, 'n'))
		args->n = 1;
	if (ft_strrchr(str, 'o'))
		args->o = 1;
}

void			stock_options(int ac, char **av, struct s_op *args)
{
	int			i;

	i = 0;
	while (av[i])
	{
		if (av[i][0] == '-')
		{
			if (ft_strrchr(av[i], 'a') || ft_strrchr(av[i], 'l')
				|| ft_strrchr(av[i], 'R') || ft_strrchr(av[i], 'r')
				|| ft_strrchr(av[i], 't'))
				pars1(args, av[i]);
			if (ft_strrchr(av[i], 'f') || ft_strrchr(av[i], 'G')
				|| ft_strrchr(av[i], 'n') || ft_strrchr(av[i], 'o'))
				pars2(args, av[i]);
			if (!check_op(&av[i][1], &args->invop))
			{
				ft_printf("ls: illegal option: -- %c\n", args->invop);
				ft_printf("usage: ls [-GRaflnort] [file...]\n");
				exit(EXIT_FAILURE);
			}
		}
		i++;
	}
}
