/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symlnk_case.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 15:05:09 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 15:09:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/ft_ls.h"

static void			put_usr_name(char **ldisp, struct stat st, struct s_op args)
{
	struct passwd	*pw;

	pw = getpwuid(st.st_uid);
	*ldisp = ft_strjoinfree(*ldisp, " ", 1);
	*ldisp = ft_strjoinfree(*ldisp, ft_itoa(st.st_nlink), 3);
	*ldisp = ft_strjoinfree(*ldisp, " ", 1);
	if (args.n)
		*ldisp = ft_strjoinfree(*ldisp, ft_itoa(pw->pw_uid), 3);
	else
		*ldisp = ft_strjoinfree(*ldisp, pw->pw_name, 1);
}

static void			put_gr_name(char **ldisp, struct stat st, struct s_op args)
{
	struct group	*gr;

	*ldisp = ft_strjoinfree(*ldisp, "  ", 1);
	if (!args.o)
	{
		gr = getgrgid(st.st_gid);
		if (args.n)
			*ldisp = ft_strjoinfree(*ldisp, ft_itoa(gr->gr_gid), 3);
		else
			*ldisp = ft_strjoinfree(*ldisp, gr->gr_name, 1);
	}
	*ldisp = ft_strjoinfree(*ldisp, "  ", 1);
}

static void			put_date(char **ldisp, struct stat st)
{
	char			*date;
	time_t			actime;

	actime = time(&actime);
	date = ctime(&st.st_mtime);
	if (st.st_mtime < actime - 15552000 || st.st_mtime > actime)
		year_date(&date, ldisp);
	else
	{
		*ldisp = ft_strjoinfree(*ldisp, &date[3], 1);
		(*ldisp)[ft_strlen(*ldisp) - 9] = '\0';
	}
}

static void			arrow_and_name(char **path)
{
	char			buf[100];

	ft_bzero(buf, 100);
	readlink(*path, buf, 100);
	buf[ft_strlen(buf)] = '\0';
	*path = ft_strjoinfree(*path, " -> ", 1);
	*path = ft_strjoinfree(*path, buf, 1);
	ft_bzero(buf, 100);
}

void				symlnk_case(struct stat st, char *path, struct s_op args)
{
	char			*ldisp;

	disp_type(&st, &ldisp);
	disp_chmod(&st, &ldisp);
	put_usr_name(&ldisp, st, args);
	put_gr_name(&ldisp, st, args);
	ldisp = ft_strjoinfree(ldisp, ft_itoa(st.st_size), 3);
	put_date(&ldisp, st);
	arrow_and_name(&path);
	ft_putstr(ldisp);
	ft_putchar(' ');
	ft_putendl(path);
}
