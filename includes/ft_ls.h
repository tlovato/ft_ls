/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 17:57:32 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/06 14:53:27 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# define _BSD_SOURCE
# define _XOPEN_SOURCE 700
# define _DARWIN_C_SOURCE

# include <dirent.h>
# include <sys/stat.h>
# include <stdio.h>
# include "libft.h"
# include "libftprintf.h"
# include <sys/types.h>
# include <stdint.h>
# include <pwd.h>
# include <grp.h>
# include <time.h>
# include <errno.h>

typedef struct		s_dir
{
	char			*name;
	char			*ldisp;
	char			*path;
	struct stat		st;
	struct s_dir	*next;
}					t_dir;

typedef struct		s_op
{
	int				l;
	int				rmaj;
	int				r;
	int				a;
	int				t;
	int				f;
	int				gmaj;
	int				n;
	int				o;
	int				invop;
	char			**path;
	struct s_dir	*lst;
	int				ndir;
	struct s_dir	*tmp;
	DIR				*dirp;
}					t_op;

typedef struct		s_all
{
	struct s_dir	*new;
	struct dirent	*dp;
	struct stat		st;
	struct stat		lst;
}					t_all;

typedef struct		s_long
{
	struct stat		st;
	struct stat		lst;
	int				total;
	int				llen;
	int				blen;
	int				ulen;
	int				glen;
	int				milen;
	int				malen;
	char			*new;
}					t_long;

struct				s_rec
{
	struct stat		st;
	DIR				*dirp;
	struct s_dir	*new;
	char			*here;
	int				condition;
}					t_rec;

/*
**	-- ft_ls.c --
*/
char				*ft_strjoinfree(char *s1, char *s2, char i);

/*
**	-- linked_list.c --
*/
struct s_dir		*takedirs(DIR *d, struct s_dir **s, struct s_op a, char *h);
struct s_dir		*take_lst(struct s_dir **list, struct s_op args);
void				stat_take_dirs(char *h, struct s_all *datas, int i);

/*
**	-- place_node.c --
*/
void				tsort2(struct s_dir *t, struct s_dir *n, struct s_dir **s);
void				time_sort1(struct s_dir *tmp, struct s_dir *new);
void				pl_node(struct s_dir **s, struct s_dir *n);
void				add_first(struct s_dir **sort, struct s_dir *new);
struct s_dir		*free_lst(struct s_dir *lst);
/*
**	-- errors.c --
*/
void				cant_open(struct stat st, char *path, struct s_op args);
void				stock_options(int ac, char **av, struct s_op *args);

/*
**	-- options.c --
*/
void				symlnk_case(struct stat st, char *path, struct s_op args);
void				pars_display(struct s_dir **s, struct s_op *a, char*p);
void				reverse(struct s_dir **sort);
void				pars_gmaj(struct s_dir *sort);
struct s_dir		*f_option(DIR *dirp, char *here);
void				rec_display(struct s_dir *s, struct s_op *a, char *p);

/*
**	-- recursive_display.c --
*/
int					cond(struct s_op *a, struct stat *s, char **h, char *n);
void				if_new(DIR *dirp, struct stat st, struct s_dir **sort);
void				initrecdata(struct s_dir **nl, char **h, char *p, char *n);
void				do_stuff(DIR *d, struct s_dir **n, struct s_op *a, char *h);

/*
**	-- utils(1, 2, 3).c --
*/
int					bl_len(struct stat st, struct s_dir *s, char *p, int n);
int					grusr_len(struct stat st, struct s_dir *s, char *p, int n);
int					minmaj_len(struct stat st, struct s_dir *s, char *p, int z);
void				put_bl_len(int l, struct stat st, struct s_dir *s, int n);
void				no_op_display(struct s_dir *sort, struct s_op *arg);
void				put_len(int len, int word, struct s_dir *sort);
void				init_args(struct s_op *args);
char				**dir_name(int ac, char **av, struct s_op *args);
void				put_name(char *name);
void				put_perm_denied(char *name);
void				init_null(struct s_dir *new);
void				pinfnorme(struct s_long *d, struct s_op a, struct s_dir *s);
int					calc_maj(int rdev);
int					calc_minmaj(int minmaj);

/*
**	-- long_display(1, 2).c --
*/
void				long_display(struct s_dir *s, char *p, struct s_op a);
void				put_last_infos(struct s_dir *sort, struct s_long *datas);
void				year_date(char **date, char **ldisp);
void				add_spaces(char **ldisp, int len, char *name);

/*
**	-- type_chmod.c --
*/
void				disp_type(struct stat *st, char **ldisp);
void				disp_chmod(struct stat *st, char **ldisp);

/*
**	-- symlnk_casec --
*/
void				symlnk_case(struct stat st, char *path, struct s_op args);

#endif
