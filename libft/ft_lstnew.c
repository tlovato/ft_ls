/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/11 14:40:16 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/14 10:56:42 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_list		*ft_lstnew(const void *content, size_t content_size)
{
	t_list	*alst;

	alst = (t_list *)malloc(sizeof(t_list));
	if (alst)
	{
		if (content == NULL)
		{
			alst->content = NULL;
			alst->content_size = 0;
		}
		else
		{
			alst->content = ft_strdup(content);
			if (!(alst->content))
			{
				free(alst);
				return (NULL);
			}
			alst->content_size = content_size;
		}
		alst->next = NULL;
	}
	return (alst);
}
