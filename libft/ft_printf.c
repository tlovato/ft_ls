/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 11:56:21 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/23 11:56:22 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

int				ft_printf(const char *restrict format, ...)
{
	va_list		ap;
	int			i;
	t_result	*ret;

	i = 0;
	ret = (t_result *)malloc(sizeof(t_result));
	attrib_values_ret(ret);
	ret->cpy = ft_strdup(format);
	va_start(ap, format);
	attrib_case(ret, ap, ret->cpy);
	ft_memprint(ret->res, ret->res_len);
	va_end(ap);
	i = ret->res_len;
	free_result(ret);
	return (i);
}
