/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   invalid_arg.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 22:23:41 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 22:23:43 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		width_invdigits(t_result *ret)
{
	char		*tmp;

	tmp = ft_strnew(ret->argum.width);
	ft_memset(tmp, ret->argum.zero ? '0' : ' ', ret->argum.width - 1);
	if (ret->argum.dash)
		tmp = ft_strcat(ret->conv, tmp);
	else
	{
		tmp = ft_strcat(tmp, ret->conv);
		free(ret->conv);
	}
	ret->conv = tmp;
}

void			inv_arg_case(t_result *ret)
{
	int			i;
	char		*tmp;

	i = 0;
	ret->conv = ft_strdup("%");
	if (ft_strlen(ret->arg) > 0)
		while (ret->arg[i])
		{
			if (is_flag(ret->arg[i]))
				attrib_flags(ret->arg, &ret->argum);
			i++;
		}
	if (ret->argum.is_width == 1)
		width_invdigits(ret);
	tmp = ft_strnew(ret->conv_len + ret->sub_len);
	ft_memcpy(tmp, ret->conv, 1);
	ft_memcpy(&tmp[1], ret->sub, ret->sub_len);
	ret->conv = tmp;
	ret->conv_len = 1 + ret->sub_len;
}
