# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/10 11:52:14 by tlovato           #+#    #+#              #
#    Updated: 2016/08/29 10:01:11 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_ls
INC_DIR = ./includes
SRC_DIR = ./srcs/
OBJ_DIR = ./objs

SRCS_FILES = errors.c ft_ls.c linked_list.c long_display.c long_display2.c options.c\
place_nodes.c recursive_display.c type_chmod.c utils.c utils2.c symlnk_case.c\
utils3.c utils4.c

SRCS = $(addprefix $(SRC_DIR), $(SRCS_FILES))

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME) : $(OBJS)
		@echo "Make objects : \033[1;32m DONE !\033[m"
		@make -C libft/
		@gcc -o $@ $^ -L libft/ -lft -fsanitize=address -g3
		@echo "Make --> $(NAME) <-- : \033[1;32m DONE !\033[m"

clean:
		@rm -f $(OBJS)
		@echo "Make clean :\033[1;31m DONE !\033[m"

fclean: clean
		@rm -f $(NAME)
		@cd libft && make fclean
		@echo "Make fclean :\033[1;31m DONE !\033[m"

re: fclean all

.PHONY: all clean fclean re
